package net.customware.confluence.plugin.toc;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ListHandlerTest {

    //The style to use when no bullet style is desired.
    private static final String NONE_STYLE = "none";

    @Test
    public void testAppendStyleEmptyValue() throws Exception {
        ListHandler listHandler = new ListHandler(null, null);

        StringBuffer out = new StringBuffer();
        listHandler.appendStyle(out);
        assertEquals("", out.toString());
    }

    @Test
    public void testAppendStyleDefaultListStyle() throws Exception {
        ListHandler listHandler = new ListHandler(AbstractTOCMacro.DEFAULT_STYLE, null);

        StringBuffer out = new StringBuffer();
        listHandler.appendStyle(out);

        assertTrue(out.toString().contains("<style type='text/css'>"));
        assertTrue(out.toString().contains("{padding: 0px;}"));
        assertTrue(out.toString().contains("ul {margin-left: 0px;}"));
        assertTrue(out.toString().contains("li {margin-left: 0px;padding-left: 0px;}"));
        assertTrue(out.toString().contains("</style>"));
    }

    @Test
    public void testAppendStyleDefaultListStyleUpperAndLowerCase() throws Exception {
        ListHandler listHandler = new ListHandler("dEfAuLt", null);

        StringBuffer out = new StringBuffer();
        listHandler.appendStyle(out);

        assertTrue(out.toString().contains("<style type='text/css'>"));
        assertTrue(out.toString().contains("{padding: 0px;}"));
        assertTrue(out.toString().contains("ul {margin-left: 0px;}"));
        assertTrue(out.toString().contains("li {margin-left: 0px;padding-left: 0px;}"));
        assertTrue(out.toString().contains("</style>"));
    }

    @Test
    public void testAppendStyleNoneListStyle() throws Exception {
        ListHandler listHandler = new ListHandler(NONE_STYLE, null);

        StringBuffer out = new StringBuffer();
        listHandler.appendStyle(out);

        assertTrue(out.toString().contains("<style type='text/css'>"));
        assertTrue(out.toString().contains("{padding: 0px;}"));
        assertTrue(out.toString().contains("ul {list-style: " + NONE_STYLE + " !important;margin-left: 0px;}"));
        assertTrue(out.toString().contains("li {margin-left: 0px;padding-left: 0px;}"));
        assertTrue(out.toString().contains("</style>"));
    }

    @Test
    public void testAppendStyleIndented() throws Exception {
        ListHandler listHandler = new ListHandler(null, "2px");

        StringBuffer out = new StringBuffer();
        listHandler.appendStyle(out);

        assertTrue(out.toString().contains("<style type='text/css'>"));
        assertTrue(out.toString().contains("{padding: 0px;}"));
        assertTrue(out.toString().contains("ul {margin-left: 0px;padding-left: 2px;}"));
        assertTrue(out.toString().contains("li {margin-left: 0px;padding-left: 0px;}"));
        assertTrue(out.toString().contains("</style>"));
    }

    @Test
    public void testAppendStyleIndentedNoneListStyle() throws Exception {
        ListHandler listHandler = new ListHandler(NONE_STYLE, "2px");

        StringBuffer out = new StringBuffer();
        listHandler.appendStyle(out);
        assertTrue(out.toString().contains("<style type='text/css'>"));
        assertTrue(out.toString().contains("{padding: 0px;}"));
        assertTrue(out.toString().contains("ul {list-style: " + NONE_STYLE + " !important;margin-left: 0px;padding-left: 2px;}"));
        assertTrue(out.toString().contains("li {margin-left: 0px;padding-left: 0px;}"));
        assertTrue(out.toString().contains("</style>"));
    }

    @Test
    public void testAppendIncLevel() throws Exception {
        ListHandler listHandler = new ListHandler(null, null);
        StringBuffer out = new StringBuffer();
        listHandler.appendIncLevel(out);
        assertEquals("\n<ul class='toc-indentation'>\n", out.toString());
    }

    @Test
    public void testAppendDecLevel() throws Exception {
        ListHandler listHandler = new ListHandler(null, null);
        StringBuffer out = new StringBuffer();
        listHandler.appendDecLevel(out);
        assertEquals("\n</ul>\n", out.toString());
    }

    @Test
    public void testAppendPrefix() throws Exception {
        ListHandler listHandler = new ListHandler(null, null);
        StringBuffer out = new StringBuffer();
        listHandler.appendPrefix(out);
        assertEquals("\n<ul class='toc-indentation'>\n", out.toString());
    }

    @Test
    public void testAppendPostfix() throws Exception {
        ListHandler listHandler = new ListHandler(null, null);
        StringBuffer out = new StringBuffer();
        listHandler.appendPostfix(out);

        // You can't write a postfix if there is no prefix written already
        assertTrue(StringUtils.isBlank(out.toString()));

        listHandler.appendPrefix(out);
        listHandler.appendPostfix(out);

        assertEquals("\n<ul class='toc-indentation'>\n\n</ul>\n", out.toString());
    }

    @Test
    public void testAppendSeparator() throws Exception {
        ListHandler listHandler = new ListHandler(null, null);
        StringBuffer out = new StringBuffer();
        listHandler.appendSeparator(out);
        assertEquals("", out.toString());
    }

    @Test
    public void testAppendHeading() throws Exception {
        ListHandler listHandler = new ListHandler(null, null);
        StringBuffer out = new StringBuffer();
        listHandler.appendHeading(out, "{token}");
        assertEquals("<li>{token}", out.toString());
    }

    @Test
    public void testStyleEncoded() throws Exception {
        ListHandler listHandler = new ListHandler("<>", null);
        StringBuffer out = new StringBuffer();
        listHandler.appendStyle(out);

        String generatedStyle = out.toString();
        assertTrue(!generatedStyle.contains("<>"));
        assertFalse(!generatedStyle.contains("list-style: \\3C \\3E"));
    }

    @Test
    public void testAlphaNumericsInStyleNotEncoded() throws Exception {
        ListHandler listHandler = new ListHandler("foobar1", null);
        StringBuffer out = new StringBuffer();
        listHandler.appendStyle(out);

        String generatedStyle = out.toString();
        assertFalse(!generatedStyle.contains("foobar1"));
    }

    @Test
    public void testIndentEncoded() throws Exception {
        ListHandler listHandler = new ListHandler(null, "<>");
        StringBuffer out = new StringBuffer();
        listHandler.appendStyle(out);

        String generatedStyle = out.toString();
        assertTrue(!generatedStyle.contains("<>"));
        assertFalse(!generatedStyle.contains("padding-left: \\3C \\3E"));
    }

    @Test
    public void testAlphaNumericsInIndentNotEncoded() throws Exception {
        ListHandler listHandler = new ListHandler(null, "foobar1");
        StringBuffer out = new StringBuffer();
        listHandler.appendStyle(out);

        String generatedStyle = out.toString();
        assertFalse(!generatedStyle.contains("foobar1"));
    }
}
