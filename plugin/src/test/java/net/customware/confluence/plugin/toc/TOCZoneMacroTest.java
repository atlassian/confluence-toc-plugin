package net.customware.confluence.plugin.toc;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.HtmlToXmlConverter;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.renderer.RenderContextOutputType;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;

@RunWith(MockitoJUnitRunner.class)
public class TOCZoneMacroTest {
    @Mock
    private XhtmlContent xhtmlContent;

    @Mock
    private HtmlToXmlConverter htmlToXmlConverter;

    @Mock
    private StaxDocumentOutlineCreator staxDocumentOutlineCreator;

    @Mock
    private SettingsManager settingsManager;

    @Mock
    private LocaleManager localeManager;

    @Mock
    private I18NBeanFactory i18nBeanFactory;

    @Mock(answer = RETURNS_DEEP_STUBS)
    private PageBuilderService pageBuilderService;

    private TOCZoneMacro tocZoneMacro;

    @Before
    public void setUp() throws Exception {
        tocZoneMacro = new TOCZoneMacro(
                staxDocumentOutlineCreator, htmlToXmlConverter, settingsManager, localeManager, i18nBeanFactory, pageBuilderService
        );
    }

    @Test
    public void bodyStillRenderedInUnprintableMode() throws Exception {
        final String macroBodyHtml = "<color=\"red\">I am the rendered body text of a macro</color>";
        final Map<String, String> macroParameters = new HashMap<String, String>();
        final Page pageBeingRendered = new Page();

        macroParameters.put("printable", Boolean.FALSE.toString());
        PageContext pageContext = pageBeingRendered.toPageContext();
        pageContext.setOutputType(RenderContextOutputType.PDF);
        assertEquals(macroBodyHtml, tocZoneMacro.execute(macroParameters, macroBodyHtml, new DefaultConversionContext(pageContext)));
    }

}
