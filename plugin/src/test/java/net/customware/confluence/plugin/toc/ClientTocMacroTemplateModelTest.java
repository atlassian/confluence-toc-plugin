package net.customware.confluence.plugin.toc;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.Map;

import static java.util.Collections.EMPTY_MAP;
import static org.hamcrest.Matchers.hasEntry;
import static org.junit.Assert.assertThat;

@SuppressWarnings("unchecked")
public class ClientTocMacroTemplateModelTest {
    @Test
    public void emptyMacroParameters() {
        final Map<String, Object> model = ClientTocMacroTemplateModel.buildTemplateModel(EMPTY_MAP);

        final Map<String, String> dataAttributes = (Map<String, String>) model.get("dataAttributes");
        assertThat(dataAttributes, hasEntry("headerelements", "H1,H2,H3,H4,H5,H6,H7"));

        assertThat(model, hasEntry("nonPrintable", (Object) false));
        assertThat(model, hasEntry("customCssClass", (Object) ""));
    }

    @Test
    public void fullySpecifiedMacroParameters() {
        Map<String, String> macroParameters = ImmutableMap.<String, String>builder()
                .put("type", "list")
                .put("outline", "true")
                .put("style", "square")
                .put("indent", "10px")
                .put("separator", "brackets")
                .put("minLevel", "2")
                .put("maxLevel", "5")
                .put("include", "foo")
                .put("exclude", "bar")
                .put("printable", "false")
                .put("class", "xxx")
                .build();
        final Map<String, Object> model = ClientTocMacroTemplateModel.buildTemplateModel(macroParameters);

        final Map<String, String> dataAttributes = (Map<String, String>) model.get("dataAttributes");
        assertThat(dataAttributes, hasEntry("headerelements", "H2,H3,H4,H5"));
        assertThat(dataAttributes, hasEntry("structure", "list"));
        assertThat(dataAttributes, hasEntry("numberedoutline", "true"));
        assertThat(dataAttributes, hasEntry("cssliststyle", "square"));
        assertThat(dataAttributes, hasEntry("csslistindent", "10px"));
        assertThat(dataAttributes, hasEntry("preseparator", "[ "));
        assertThat(dataAttributes, hasEntry("midseparator", " ] [ "));
        assertThat(dataAttributes, hasEntry("postseparator", " ]"));
        assertThat(dataAttributes, hasEntry("includeheaderregex", "foo"));
        assertThat(dataAttributes, hasEntry("excludeheaderregex", "bar"));

        assertThat(model, hasEntry("nonPrintable", (Object) true));
        assertThat(model, hasEntry("customCssClass", (Object) "xxx"));
    }

    @Test
    public void minAndMaxLevelParametersAreLimited() {
        Map<String, String> macroParameters = ImmutableMap.<String, String>builder()
                .put("minlevel", "-1")
                .put("maxLevel", "10")
                .build();
        final Map<String, Object> model = ClientTocMacroTemplateModel.buildTemplateModel(macroParameters);

        final Map<String, String> dataAttributes = (Map<String, String>) model.get("dataAttributes");
        assertThat(dataAttributes, hasEntry("headerelements", "H1,H2,H3,H4,H5,H6,H7"));

    }

}