package net.customware.confluence.plugin.toc;

import net.customware.confluence.plugin.toc.DocumentOutline.Heading;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Most of the tests here rely on the DocumentOutline implementation to assert correctness. Ensure
 * {@link DocumentOutlineImplTest} passes.
 */
public class DefaultDocumentOutlineBuilderTest {
    private DefaultDocumentOutlineBuilder builder;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        builder = new DefaultDocumentOutlineBuilder();
    }

    @Test
    public void testNoBuilding() {
        Iterator<Heading> iterator = builder.getDocumentOutline().iterator();
        assertFalse(iterator.hasNext());
    }

    @Test
    public void testAddTopLevelItemsOnly() {
        Iterator<Heading> iterator = builder.add("a", null, 1).add("b", null, 1).add("c", null, 1).getDocumentOutline().iterator();
        assertEquals("a", iterator.next().getName());
        assertEquals("b", iterator.next().getName());
        assertEquals("c", iterator.next().getName());
    }

    @Test
    public void testAddToNextLevelWhenNoHeadings() {
        Iterator<Heading> iterator = builder.nextLevel().add("a", null, 1).getDocumentOutline().iterator();

        assertEquals("a", iterator.next().getName());
        assertFalse(iterator.hasNext());
    }

    @Test
    public void testNextLevel() {
        Iterator<Heading> iterator = builder.add("1-1", null, 1).nextLevel().add("2-1", null, 2).nextLevel().add("3-1", null, 3).nextLevel().add(
                "4-1", null, 4).getDocumentOutline().iterator();
        assertEquals("1-1", iterator.next().getName());
        assertEquals("2-1", iterator.next().getName());
        assertEquals("3-1", iterator.next().getName());
        assertEquals("4-1", iterator.next().getName());
    }

    @Test
    public void testMissingNextLevels() {
        Iterator<Heading> iterator = builder.add("top", null, 1).nextLevel().nextLevel().nextLevel().add("bottom", null, 4)
                .getDocumentOutline().iterator();

        assertEquals("top", iterator.next().getName());
        assertEquals("bottom", iterator.next().getName());
        assertFalse(iterator.hasNext());
    }

    @Test
    public void testAllNextLevel() {
        Iterator<Heading> iterator = builder.nextLevel().nextLevel().nextLevel().nextLevel().nextLevel()
                .getDocumentOutline().iterator();

        assertFalse(iterator.hasNext());
    }

    @Test
    public void testPreviousLevelWhenAtTop() {
        expectedException.expect(IllegalStateException.class);
        builder.previousLevel();
    }

    @Test
    public void testPreviousLevel() {
        Iterator<Heading> iterator = builder.add("1-1", null, 1).nextLevel().add("2-1", null, 2).previousLevel().add("1-2", null, 1).nextLevel()
                .add("2-2", null, 2).nextLevel().add("3-1", null, 3).previousLevel().add("2-3", null, 2).previousLevel().add("1-3", null, 1)
                .getDocumentOutline().iterator();

        assertEquals("1-1", iterator.next().getName());
        assertEquals("2-1", iterator.next().getName());
        assertEquals("1-2", iterator.next().getName());
        assertEquals("2-2", iterator.next().getName());
        assertEquals("3-1", iterator.next().getName());
        assertEquals("2-3", iterator.next().getName());
        assertEquals("1-3", iterator.next().getName());

        expectedException.expect(IllegalStateException.class);
        builder.previousLevel();
    }

    @Test
    public void testMultiplePreviousLevel() {
        Iterator<Heading> iterator = builder.add("1-1", null, 1).nextLevel().add("2-1", null, 2).nextLevel().add("3-1", null, 3).nextLevel().add(
                "4-1", null, 4).previousLevel().previousLevel().previousLevel().add("1-2", null, 1).getDocumentOutline().iterator();

        assertEquals("1-1", iterator.next().getName());
        assertEquals("2-1", iterator.next().getName());
        assertEquals("3-1", iterator.next().getName());
        assertEquals("4-1", iterator.next().getName());
        assertEquals("1-2", iterator.next().getName());
    }
}
