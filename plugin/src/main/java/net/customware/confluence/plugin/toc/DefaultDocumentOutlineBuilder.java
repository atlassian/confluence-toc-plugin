package net.customware.confluence.plugin.toc;

import net.customware.confluence.plugin.toc.DocumentOutline.Heading;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

/**
 * The default outline builder.
 */
public class DefaultDocumentOutlineBuilder implements DepthFirstDocumentOutlineBuilder {
    /**
     * The top level items in the outline.
     */
    protected List<BuildableHeading> topOfOutline;

    /**
     * The ancestry for the current level of the document we are building. The top of this stack is the parent of
     * the current level being built. If the stack is empty then we are currently building the top of the outline.
     */
    private Stack<BuildableHeading> ancestors;

    public DefaultDocumentOutlineBuilder() {
        this.topOfOutline = new ArrayList<BuildableHeading>();
        this.ancestors = new Stack<BuildableHeading>();
    }

    @Override
    public DepthFirstDocumentOutlineBuilder add(String name, String anchor, int type) {
        BuildableHeading heading = new BuildableHeading(name, anchor, type);

        if (ancestors.empty()) {
            topOfOutline.add(heading);
        } else {
            ancestors.peek().addChild(heading);
        }

        return this;
    }

    @Override
    public DepthFirstDocumentOutlineBuilder nextLevel() {
        if (ancestors.isEmpty()) {
            // Current building the top of the document outline.
            // If there are no top level elements yet then add a place holder one.
            if (topOfOutline.isEmpty())
                addPlaceholder();

            // Make the last added top level item an ancestor (so future 'addHeading' calls add beneath it).
            ancestors.push(topOfOutline.get(topOfOutline.size() - 1));
        } else {
            // the last child of the current ancestor becomes the new ancestor for future adds
            BuildableHeading currentAncestor = ancestors.peek();

            // If there are no entries on this level currently so add a place holder
            if (!currentAncestor.hasChildren())
                addPlaceholder();

            ancestors.push(currentAncestor.getLastChild());
        }

        return this;
    }

    @Override
    public DepthFirstDocumentOutlineBuilder previousLevel() {
        if (ancestors.isEmpty())
            throw new IllegalStateException("Already building the top level of the document.");

        ancestors.pop();

        return this;
    }

    @Override
    public DocumentOutline getDocumentOutline() {
        return new DocumentOutlineImpl(topOfOutline);
    }

    private void addPlaceholder() {
        add(null, null, 0);
    }

    static class BuildableHeading implements Heading {
        private final String name;
        private final String anchor;
        private final List<BuildableHeading> children;
        private BuildableHeading parent;
        private final int type;

        BuildableHeading(String name, String anchor, int type) {
            this.name = name;
            this.anchor = anchor;
            this.children = new ArrayList<BuildableHeading>();
            this.parent = null;
            this.type = type;
        }

        void addChild(BuildableHeading child) {
            child.setParent(this);
            children.add(child);
        }

        void addChildren(Collection<BuildableHeading> children) {
            for (BuildableHeading child : children)
                addChild(child);
        }

        BuildableHeading getLastChild() {
            if (this.children.isEmpty())
                return null;

            return children.get(this.children.size() - 1);
        }

        List<BuildableHeading> getChildren() {
            return Collections.unmodifiableList(children);
        }

        void clearChildren() {
            children.clear();
        }

        void setParent(BuildableHeading parent) {
            this.parent = parent;
        }

        boolean hasChildren() {
            return !children.isEmpty();
        }

        /**
         * @param index child index
         * @return
         * @throws IndexOutOfBoundsException if there is no child with that index.
         */
        public BuildableHeading getChild(int index) {
            return children.get(index);
        }

        @Override
        public int getChildCount() {
            return children.size();
        }

        @Override
        public int getEffectiveLevel() {
            int level = 1;

            BuildableHeading ancestor = parent;
            while (ancestor != null) {
                level++;
                ancestor = ancestor.getParent();
            }

            return level;
        }

        @Override
        public int getType() {
            return type;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String getAnchor() {
            return anchor;
        }

        public BuildableHeading getParent() {
            return parent;
        }

        @Override
        public String toString() {
            return "BuildableHeading[" + ((name != null) ? name : "_placeholder_") + "]";
        }
    }
}
