package net.customware.confluence.plugin.toc.spring;

import com.atlassian.confluence.content.render.xhtml.HtmlToXmlConverter;
import com.atlassian.confluence.content.render.xhtml.XmlEventReaderFactory;
import com.atlassian.confluence.content.render.xhtml.XmlOutputFactory;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.xhtml.MacroMigration;
import com.atlassian.confluence.renderer.template.TemplateRenderer;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

/**
 * Spring beans for the OSGi services imported by this app.
 */
@Configuration
public class ImportedOsgiServices {

    @Bean
    public XmlEventReaderFactory xmlEventReaderFactory() {
        return importOsgiService(XmlEventReaderFactory.class);
    }

    @Bean
    public XmlOutputFactory xmlFragmentOutputFactory() {
        return importOsgiService(XmlOutputFactory.class);
    }

    @Bean
    public XhtmlContent xhtmlContent() {
        return importOsgiService(XhtmlContent.class);
    }

    @Bean
    public TemplateRenderer templateRenderer() {
        return importOsgiService(TemplateRenderer.class);
    }

    @Bean
    public EventPublisher eventPublisher() {
        return importOsgiService(EventPublisher.class);
    }

    @Bean
    public MacroMigration richTextMacroMigration() {
        return importOsgiService(MacroMigration.class);
    }

    @Bean
    public HtmlToXmlConverter htmlToXmlConverter() {
        return importOsgiService(HtmlToXmlConverter.class);
    }

    @Bean
    public SettingsManager settingsManager() {
        return importOsgiService(SettingsManager.class);
    }

    @Bean
    public LocaleManager localeManager() {
        return importOsgiService(LocaleManager.class);
    }

    @Bean
    public I18NBeanFactory i18nBeanFactory() {
        return importOsgiService(I18NBeanFactory.class);
    }

    @Bean
    public PageBuilderService pageBuilderService() {
        return importOsgiService(PageBuilderService.class);
    }
}
