package net.customware.confluence.plugin.toc.spring;

import com.atlassian.confluence.content.render.xhtml.XmlEventReaderFactory;
import com.atlassian.confluence.content.render.xhtml.XmlOutputFactory;
import net.customware.confluence.plugin.toc.StaxDocumentOutlineCreator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * The Spring beans for this app.
 */
@Configuration
@Import(ImportedOsgiServices.class)
public class SpringBeans {

    // --------------------- Internal components ---------------------

    @Bean
    public StaxDocumentOutlineCreator staxDocumentOutlineCreator(
            final XmlEventReaderFactory xmlEventReaderFactory,
            final XmlOutputFactory xmlFragmentOutputFactory) {
        return new StaxDocumentOutlineCreator(xmlEventReaderFactory, xmlFragmentOutputFactory);
    }
}
