package net.customware.confluence.plugin.toc;

/**
 * Implemented by a class that can build a Document outline "depth first". That is you can increase and decrease the
 * depth that entries are being added.
 */
public interface DepthFirstDocumentOutlineBuilder {
    /**
     * Add a heading to the current level in the document
     *
     * @param name   heading name
     * @param anchor heading anchor
     * @param type   the type of the heading e.g. an h1 is type 1.
     */
    public DepthFirstDocumentOutlineBuilder add(String name, String anchor, int type);

    /**
     * Move the builder to the next level so that the next call to add is to a lower level than the previous.
     */
    public DepthFirstDocumentOutlineBuilder nextLevel();

    /**
     * Move the builder to the previous level so that the next call to add is to a higher level than the previous.
     */
    public DepthFirstDocumentOutlineBuilder previousLevel();

    /**
     * @return the DocumentOutline that has been built.
     */
    public DocumentOutline getDocumentOutline();
}
