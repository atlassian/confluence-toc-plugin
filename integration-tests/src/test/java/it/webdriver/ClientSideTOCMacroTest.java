package it.webdriver;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentType;
import com.atlassian.confluence.test.rest.api.ConfluenceRestClient;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import it.net.customware.confluence.plugin.toc.Heading;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import static com.atlassian.confluence.api.model.content.ContentRepresentation.WIKI;
import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.REGULAR_PERMISSIONS;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static it.webdriver.TableOfContents.Style.FLAT;
import static it.webdriver.TableOfContents.Style.LIST;
import static org.apache.commons.lang3.StringUtils.deleteWhitespace;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Tests for conditions where the TOC macro client-side implementation is used.
 */
@RunWith(ConfluenceStatelessTestRunner.class)
public class ClientSideTOCMacroTest {

    @Inject
    private static ConfluenceTestedProduct product;
    @Inject
    private static ConfluenceRestClient restClient;

    @Fixture
    private static UserFixture user = userFixture().build();

    @Fixture
    private static SpaceFixture space = spaceFixture()
            .permission(user, REGULAR_PERMISSIONS)
            .build();

    @Rule
    public TestName testName = new TestName();

    private Content page;

    @Test
    public void tocWithNonSequentialHeadersStartingFromH2() {
        createPage("{toc}\n" +
                "h2. Heading 1\n" +
                "h4. Heading 1.1\n" +
                "h4. Heading 1.2\n" +
                "h6. Heading 1.2.1\n" +
                "h2. Heading 2\n" +
                "h4. Heading 2.1\n");

        visitPageAndAssertTocHeadings(LIST,
                heading("Heading 1",
                        heading("Heading 1.1"),
                        heading("Heading 1.2",
                                heading("Heading 1.2.1")
                        )
                ),
                heading("Heading 2",
                        heading("Heading 2.1")
                )
        );
    }

    @Test
    public void outlinedTocWithNonSequentialHeadersStartingFromH2() {
        createPage(
                "{toc:outline=true}\n" +
                        "h2. Heading 1\n" +
                        "h4. Heading 1.1\n" +
                        "h4. Heading 1.2\n" +
                        "h6. Heading 1.1.1\n" +
                        "h2. Heading 2\n" +
                        "h4. Heading 2.1\n");

        visitPageAndAssertTocHeadings(LIST,
                outlinedHeading("1", "Heading 1",
                        outlinedHeading("1.1", "Heading 1.1"),
                        outlinedHeading("1.2", "Heading 1.2",
                                outlinedHeading("1.2.1", "Heading 1.1.1")
                        )
                ),
                outlinedHeading("2", "Heading 2",
                        outlinedHeading("2.1", "Heading 2.1")
                )
        );
    }

    @Test
    public void tocWithHeadingsContainingWikiMarkup() {
        createPage("{toc}\n" +
                "h2. [Confluence Overview|ds:Confluence Overview]\n" +
                "h4. {color:red}Red heading{color}\n"
        );

        visitPageAndAssertTocHeadings(LIST,
                heading("Confluence Overview",
                        heading("Red heading")
                ));
    }

    @Test
    public void tocWithHeadingsExcluded() {
        createPage("{toc:exclude=Heading two}\n" +
                "h1. Heading one\n" +
                "h1. Heading two\n"
        );

        visitPageAndAssertTocHeadings(LIST,
                heading("Heading one")
        );
    }

    @Test
    public void tocWithHeadingsIncluded() {
        createPage("{toc:include=Heading one}\n" +
                "h1. Heading one\n" +
                "h1. Heading two\n"
        );

        visitPageAndAssertTocHeadings(LIST,
                heading("Heading one")
        );
    }

    @Test
    public void tocWithCssClassSpecified() {
        createPage("{toc:class=foobar}\n" +
                "h2. Heading 1\n" +
                "h4. Heading 1.1\n" +
                "h4. Heading 1.2\n" +
                "h6. Heading 1.2.1\n" +
                "h2. Heading 2\n" +
                "h4. Heading 2.1\n"
        );

        final TableOfContents tableOfContents = visitPageAndFindToc(page, LIST);
        assertThat(tableOfContents.getTocContainer().hasClass("foobar"), is(true));

        assertTocHeadings(
                tableOfContents,
                heading("Heading 1",
                        heading("Heading 1.1"),
                        heading("Heading 1.2",
                                heading("Heading 1.2.1")
                        )
                ),
                heading("Heading 2",
                        heading("Heading 2.1")
                )
        );
    }

    @Test
    public void flatTocWithNonSequentialHeadersStartingFromH2() {
        createPage("{toc:type=flat}\n" +
                "h2. Heading 1\n" +
                "h4. Heading 1.1\n" +
                "h4. Heading 1.2\n" +
                "h6. Heading 1.2.1\n" +
                "h2. Heading 2\n" +
                "h4. Heading 2.1\n");

        visitPageAndAssertTocHeadings(FLAT,
                heading("Heading 1"),
                heading("Heading 1.1"),
                heading("Heading 1.2"),
                heading("Heading 1.2.1"),
                heading("Heading 2"),
                heading("Heading 2.1")
        );
    }

    @Test
    public void flatOutlineTocWithNonSequentialHeadersStartingFromH2() {
        createPage("{toc:type=flat|outline=true}\n" +
                "h2. Heading 1\n" +
                "h4. Heading 1.1\n" +
                "h4. Heading 1.2\n" +
                "h6. Heading 1.2.1\n" +
                "h2. Heading 2\n" +
                "h4. Heading 2.1\n");

        visitPageAndAssertTocHeadings(FLAT,
                outlinedHeading("1", "Heading 1"),
                outlinedHeading("1.1", "Heading 1.1"),
                outlinedHeading("1.2", "Heading 1.2"),
                outlinedHeading("1.2.1", "Heading 1.2.1"),
                outlinedHeading("2", "Heading 2"),
                outlinedHeading("2.1", "Heading 2.1")
        );
    }

    @Test
    public void flatTocWithDefaultSeparator() {
        createPage("{toc:type=flat}\n" +
                "h1. Heading one\n" +
                "h1. Heading two\n"
        );

        final TableOfContents tableOfContents = visitPageAndFindToc(page, FLAT);

        assertTocHeadings(tableOfContents,
                heading("Heading one"),
                heading("Heading two")
        );

        assertThat(tableOfContents.getTocContainer().getText(), is("[ Heading one ] [ Heading two ]"));
    }

    @Test
    public void flatTocWithBraceSeparator() {
        createPage("{toc:type=flat|separator=brace}\n" +
                "h1. Heading one\n" +
                "h1. Heading two\n"
        );

        final TableOfContents tableOfContents = visitPageAndFindToc(page, FLAT);

        assertThat(tableOfContents.getTocContainer().getText(), is("{ Heading one } { Heading two }"));
    }

    @Test
    public void flatTocWithCommaSeparator() {
        createPage("{toc:type=flat|separator=comma}\n" +
                "h1. Heading one\n" +
                "h1. Heading two\n"
        );

        final TableOfContents tableOfContents = visitPageAndFindToc(page, FLAT);

        assertThat(tableOfContents.getTocContainer().getText(), is("Heading one , Heading two"));
    }

    @Test
    public void flatTocWithCustomSeparator() {
        createPage("{toc:type=flat|separator=++}\n" +
                "h1. Heading one\n" +
                "h1. Heading two\n"
        );

        final TableOfContents tableOfContents = visitPageAndFindToc(page, FLAT);

        assertThat(tableOfContents.getTocContainer().getText(), is("Heading one++Heading two"));
    }

    @Test
    public void tocWithMinAndMaxLevelRange() {
        createPage("{toc:type=list|minLevel=3|maxLevel=4}\n" +
                "h2. Heading two\n" +
                "h3. Heading three once\n" +
                "h3. Heading three again\n" +
                "h4. Heading four\n" +
                "h5. Heading five\n"
        );

        visitPageAndAssertTocHeadings(LIST,
                heading("Heading three once"),
                heading("Heading three again",
                        heading("Heading four")
                )
        );
    }

    @Test
    public void tocWithMinAndMaxLevelSingleValue() {
        createPage("{toc:type=list|minLevel=3|maxLevel=3}\n" +
                "h2. Heading two\n" +
                "h3. Heading three once\n" +
                "h3. Heading three again\n" +
                "h4. Heading four\n" +
                "h5. Heading five\n"
        );

        visitPageAndAssertTocHeadings(LIST,
                heading("Heading three once"),
                heading("Heading three again")
        );
    }

    @Test
    public void tocWithMinLevelOnly() {
        createPage("{toc:type=list|minLevel=3}\n" +
                "h2. Heading two\n" +
                "h3. Heading three once\n" +
                "h3. Heading three again\n" +
                "h4. Heading four\n" +
                "h5. Heading five\n"
        );

        visitPageAndAssertTocHeadings(LIST,
                heading("Heading three once"),
                heading("Heading three again",
                        heading("Heading four",
                                heading("Heading five")
                        )
                )
        );
    }

    @Test
    public void tocWithMaxLevelOnly() {
        createPage("{toc:type=list|maxLevel=4}\n" +
                "h2. Heading two\n" +
                "h3. Heading three once\n" +
                "h3. Heading three again\n" +
                "h4. Heading four\n" +
                "h5. Heading five\n"
        );

        visitPageAndAssertTocHeadings(LIST,
                heading("Heading two",
                        heading("Heading three once"),
                        heading("Heading three again",
                                heading("Heading four")
                        )
                )
        );
    }

    private void visitPageAndAssertTocHeadings(final TableOfContents.Style tocStyle, final Heading... headings) {
        final TableOfContents tableOfContents = visitPageAndFindToc(page, tocStyle);
        assertTocHeadings(tableOfContents, headings);
    }

    private static void assertTocHeadings(final TableOfContents tableOfContents, final Heading... headings) {
        assertThat("Unexpected TOC headings", tableOfContents.getHeadings(), contains(
                headings
        ));
    }

    private Heading heading(final String text, final Heading... children) {
        return new Heading(text, anchor(deleteWhitespace(text)), children);
    }

    private Heading outlinedHeading(final String outline, final String text, final Heading... children) {
        return new Heading(outline, text, anchor(deleteWhitespace(text)), children);
    }

    private TableOfContents visitPageAndFindToc(final Content page, final TableOfContents.Style tocStyle) {
        product.loginAndView(user.get(), page);
        return product.getPageBinder().bind(TableOfContents.class, tocStyle);
    }

    private void createPage(final String content) {
        page = restClient.createSession(user.get()).contentService().create(
                Content.builder(ContentType.PAGE)
                        .title(testName.getMethodName())
                        .body(content, WIKI)
                        .space(space.get())
                        .build());
    }

    private String anchor(String heading) {
        return String.format("%s-%s", page.getTitle(), heading);
    }
}
