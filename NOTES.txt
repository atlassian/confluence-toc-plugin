Release Notes
------------

v1.4.6 - 11-Oct-2005 - David Peterson
* Updated for 1.4/1.5 compatibility
* Now requires the 'Utilities Plugin' to be installed.
* Added a 'class' option to allow external CSS customisation.
* The options for displaying as a list vs flat and outline have changed. See the docs for details.

v1.4.5 - 19-Sep-2005 - David Peterson
* Added a 'printable' parameter which if set to 'false' will hide the TOC when in 'print' mode.

v1.4.4 - 13-Sep-2005 - David Peterson
* Fixed a bug when outputing an empty TOC in 'flat' mode.
* Added a work-around for using '|' in a filter. Use '//' where you would usually use '|'.

v1.4.3 - 09-Sep-2005 - David Peterson
* Added the {toc-zone} macro, which only lists headings which occur inside its body.
* Added the 'filter' parameter, allowing regex filtering of allowed headings.
* Added the 'flat' style, which outputs each link on a single line, separated by standard characters.
* Added the 'separator' parameter, which allows control over which characters separate links when in 'flat' style.

v1.4.2 - 11-Aug-2005 - David Peterson 	
* Updated to use 1.4 renderer.
* Fixed bug with styles being inherited from previous TOC if more than one exists on a single page.
* Fixed bug when being rendered on Templates or when previewing a new page.
* Renders all macro/markup content that exists in the headings. WARNING!!! May have side effects!
* Now optionally includes headings in included pages in the TOC.
* Added 'outline' style which outputs subheadings in 1.2.3 style.
* Added the 'minLevel' parameter.
* Renamed the 'depth' parameter to 'maxLevel'.

v1.0 - 25-Apr-2005 - David Peterson 	
* Original release.
